package com.kenfogel.casinodice.tests;

import com.kenfogel.casinodice.business.PassLine;
import com.kenfogel.casinodice.enums.OutComes;
import java.util.Arrays;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Parameterized test for PassLine
 *
 * @author Ken Fogel
 */
@RunWith(Parameterized.class)
public class PassLineTest {

    /**
     * A static method is required to hold all the data to be tested and the
     * expected results for each test. This data must be stored in a
     * two-dimension array. The 'name' attribute of Parameters is a JUnit 4.11
     * feature
     *
     * @return The list of arrays
     */
    @Parameters(name = "Does a roll of {0} give an outcome of {1}? Set # {index}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {7, OutComes.WIN_SINGLE},
            {11, OutComes.WIN_SINGLE},
            {2, OutComes.LOSE},
            {3, OutComes.LOSE},
            {12, OutComes.LOSE},
            {4, OutComes.ROLL_AGAIN},
            {5, OutComes.ROLL_AGAIN},
            {6, OutComes.ROLL_AGAIN},
            {8, OutComes.ROLL_AGAIN},
            {9, OutComes.ROLL_AGAIN},
            {10, OutComes.ROLL_AGAIN}
        });
    }

    private final int rollOfTheDice;
    private final OutComes outcome;

    /**
     * Constructor required by the Parameterized.class
     *
     * @param rollOfTheDice
     * @param outcome
     */
    public PassLineTest(int rollOfTheDice, OutComes outcome) {
        this.rollOfTheDice = rollOfTheDice;
        this.outcome = outcome;
    }

    /**
     * Test of playPassLine method, of class PassLine.
     */
    @Test
    public void testPlayPassLine() {
        PassLine instance = new PassLine();
        OutComes result = instance.playPassLine(rollOfTheDice);
        assertEquals(outcome, result);
    }
}

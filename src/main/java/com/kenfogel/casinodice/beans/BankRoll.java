package com.kenfogel.casinodice.beans;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Objects;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class holds the identity of the playery and how much money the player
 * has in a game.
 *
 * Added player name and email address
 *
 * @author Ken
 * @version 1.1
 */
public class BankRoll {

    private final ObjectProperty<BigDecimal> money;
    private final ObjectProperty<BigDecimal> moneyBet;
    private final StringProperty playerName;
    private final StringProperty playerEmailAddress;

    public BankRoll() {
        money = new SimpleObjectProperty<>(BigDecimal.ZERO);
        moneyBet = new SimpleObjectProperty<>(BigDecimal.ZERO);
        playerName = new SimpleStringProperty("");
        playerEmailAddress = new SimpleStringProperty("");
    }

    public BigDecimal getMoney() {
        return money.get();
    }

    public void setMoney(BigDecimal money) {
        this.money.set(money);
    }

    public ObjectProperty<BigDecimal> moneyProperty() {
        return money;
    }

    public BigDecimal getMoneyBet() {
        return moneyBet.get();
    }

    public void setMoneyBet(BigDecimal moneyBet) {
        this.moneyBet.set(moneyBet);
    }

    public ObjectProperty<BigDecimal> moneyBetProperty() {
        return moneyBet;
    }
    
    public String getPlayerName() {
        return playerName.get();
    }

    public void setPlayerName(String playerName) {
        this.playerName.set(playerName);
    }

    public StringProperty playerNameProperty() {
        return playerName;
    }

    public String getPlayerEmailAddress() {
        return playerEmailAddress.get();
    }

    public void setPlayerEmailAddress(String playerEmailAddress) {
        this.playerEmailAddress.set(playerEmailAddress);
    }

    public StringProperty playerEmailAddressProperty() {
        return playerEmailAddress;
    }

    /**
     * Add the amount to the bankroll when the player wins a bet
     *
     * @param amount
     */
    public void add(BigDecimal amount) {
        money.set(money.get().add(amount));
    }

    /**
     * Subtract the amount to the bankroll when the player looses a bet
     *
     * @param amount
     */
    public void subtract(BigDecimal amount) {
        money.set(money.get().subtract(amount));

    }

    /**
     * Returns the monet available as a currency formatted string
     *
     * @return current amount of money
     */
    public String getFormattedMoney() {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        return nf.format(money.get().doubleValue());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.money.get());
        hash = 67 * hash + Objects.hashCode(this.moneyBet.get());
        hash = 67 * hash + Objects.hashCode(this.playerName.get());
        hash = 67 * hash + Objects.hashCode(this.playerEmailAddress.get());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BankRoll other = (BankRoll) obj;
        if (!Objects.equals(this.playerName.get(), other.playerName.get())) {
            return false;
        }
        if (!Objects.equals(this.playerEmailAddress.get(), other.playerEmailAddress.get())) {
            return false;
        }
        if (!Objects.equals(this.moneyBet.get(), other.moneyBet.get())) {
            return false;
        }
        return Objects.equals(this.money.get(), other.money.get());
    }
    
    @Override
    public String toString() {
        return playerName.get() + "  " + playerEmailAddress.get() + "  " + moneyBet.get() + "  " + getFormattedMoney();
    }
}

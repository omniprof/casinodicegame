package com.kenfogel.casinodice.beans;

import java.util.Random;

/**
 * This class represents a gaming die that generates a random number from 1 - 6.
 *
 * @author Ken
 * @version 1.0
 *
 */
public final class Die {

    private int oneDie;
    private final Random random;

    public Die() {
        random = new Random();
        rollTheDie();
    }

    public int getDie() {
        return oneDie;
    }

    public int rollTheDie() {
        // Create an seed a random number generator
        oneDie = random.nextInt(6) + 1;
        return oneDie;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + this.oneDie;
        return hash;
    }

}

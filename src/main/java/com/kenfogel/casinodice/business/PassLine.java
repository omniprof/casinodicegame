package com.kenfogel.casinodice.business;

import com.kenfogel.casinodice.enums.OutComes;

/**
 * Pass Line Dice Game
 *
 * @author kfogel
 */
public class PassLine {

    private int point;

    /**
     * First Roll of the dice
     *
     * @param firstRollOfDice
     * @return the outcome of the roll
     */
    public OutComes playPassLine(int firstRollOfDice) {

        OutComes outcome;
        point = -1;

        // The new and improved switch expression available in Java 14
        outcome = switch (firstRollOfDice) {
            case 7, 11 ->
                OutComes.WIN_SINGLE;
            case 2, 3, 12 ->
                OutComes.LOSE;
            default -> {
                point = firstRollOfDice;
                yield (OutComes.ROLL_AGAIN); // yield allows a value to be 
            }                                // returned from a block
        };

        // Old style switch
//        switch (firstRollOfDice) {
//            case 7:
//            case 11:
//                outcome = OutComes.WIN_SINGLE;
//                break;
//            case 2:
//            case 3:
//            case 12:
//                outcome = OutComes.LOSE;
//                break;
//            default:
//                point = firstRollOfDice;
//                outcome = OutComes.ROLL_AGAIN;
//        }
        return outcome;
    }

    /**
     * Second roll of the dice
     * @param subsequentRollOfDice
     * @return outcome of the roll
     */
    public OutComes playForPoint(int subsequentRollOfDice) {
        OutComes outcome;

        if (point == -1) { // calling this method before playPassLine
            outcome = OutComes.OUT_OF_ORDER;
        } else {
            if (point == subsequentRollOfDice) {
                outcome = OutComes.WIN_SINGLE;
            } else {
                if (subsequentRollOfDice == 7) {
                    outcome = OutComes.LOSE;
                } else {
                    outcome = OutComes.ROLL_AGAIN;
                }
            }
        }
        return outcome;
    }
}

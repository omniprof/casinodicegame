package com.kenfogel.casinodice.business;

import com.kenfogel.casinodice.enums.OutComes;

/**
 * Field Bet Dice game
 *
 * @author Ken Fogel
 */
public class FieldBet {

    public OutComes playFieldBet(int totalDie) {

        OutComes outcome;

        // The new and improved switch expression available in Java 14
        outcome = switch (totalDie) {
            case 2 ->
                OutComes.WIN_DOUBLE;
            case 3, 4, 9, 10, 11 ->
                OutComes.WIN_SINGLE;
            case 12 ->
                OutComes.WIN_TRIPLE;
            default ->
                OutComes.LOSE;
        };

        // Old style switch
//        switch (totalDie) {
//            case 2:
//                outcome = OutComes.WIN_DOUBLE;
//                break;
//            case 3:
//            case 4:
//            case 9:
//            case 10:
//            case 11:
//                outcome = OutComes.WIN_SINGLE;
//                break;
//            case 12:
//                outcome = OutComes.WIN_TRIPLE;
//                break;
//            default:
//                outcome = OutComes.LOSE;
//                break;
//        }
        return outcome;
    }
}

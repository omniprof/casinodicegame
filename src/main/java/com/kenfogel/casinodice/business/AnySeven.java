package com.kenfogel.casinodice.business;

import com.kenfogel.casinodice.enums.OutComes;

/**
 * AnySeven dice game
 *
 * @author Ken Fogel
 */
public class AnySeven {

    public OutComes playAnySeven(int totalDie) {
        if (totalDie == 7) {
            return OutComes.WIN_QUADRUPLE;
        } else {
            return OutComes.LOSE;
        }
    }
}

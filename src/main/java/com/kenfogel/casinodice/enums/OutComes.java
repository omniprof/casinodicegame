package com.kenfogel.casinodice.enums;

/**
 * An enum or enumeration allows us to give a name to a value. If there are no
 * values shown then the numbering begins at zero.
 *
 * @author kfogel
 */
public enum OutComes {

    WIN_SINGLE, WIN_DOUBLE, WIN_TRIPLE, WIN_QUADRUPLE, LOSE, ROLL_AGAIN, OUT_OF_ORDER;
}

package com.kenfogel.casinodice.app;

import java.io.IOException;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a Casino Dice game
 *
 * @author Ken Fogel
 */
public class MainApp extends Application {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    // The primary window or frame of this application
    private Stage primaryStage;
    
    //private BankRoll bankRoll;

    @Override
    public void start(Stage primaryStage) throws Exception {
        LOG.info("Program Begins");
        //bankRoll = new BankRoll();
        // The Stage comes from the framework so make a copy to use elsewhere
        this.primaryStage = primaryStage;
        // Create the Scene and put it on the Stage
        configureStage();

        // Set the window title
        primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("TITLE"));
        // Raise the curtain on the Stage
        primaryStage.show();
    }

    /**
     * Load the FXML and bundle, create a Scene and put the Scene on Stage.
     */
    private void configureStage() {
        try {
            // Instantiate the FXMLLoader
            FXMLLoader loader = new FXMLLoader();

            // Set the location of the fxml file in the FXMLLoader
            loader.setLocation(MainApp.class.getResource("/fxml/CasinoFXML.fxml"));

            // Localize the loader with its bundle
            // Uses the default locale and if a matching bundle is not found
            // will then use MessagesBundle.properties
            loader.setResources(ResourceBundle.getBundle("MessagesBundle"));

            // Parent is the base class for all nodes that have children in the
            // scene graph such as AnchorPane and most other containers
            Parent parent = (GridPane) loader.load();

            // Load the parent into a Scene
            Scene scene = new Scene(parent);

            // Put the Scene on Stage
            primaryStage.setScene(scene);

            // Retrieve a reference to the controller so that you can pass a
            // reference to the persistence object and the bean
            // Only used if its necessary to pass data to the controller from outside
            //CasinoFXMLController controller = loader.getController();
            //controller.setBankRoll(bankRoll);
            
        } catch (IOException ex) { // getting resources or files
            // could fail
            LOG.error("Could not load fxml, message bundle, or css files", ex);
            errorAlert(ex.getMessage());
            System.exit(1);
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("ErrorStartTitle"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessagesBundle").getString("ErrorStartText"));
        dialog.setContentText(msg);
        dialog.show();
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

}

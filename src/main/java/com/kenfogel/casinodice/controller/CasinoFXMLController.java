package com.kenfogel.casinodice.controller;

import com.kenfogel.casinodice.beans.BankRoll;
import com.kenfogel.casinodice.beans.Die;
import com.kenfogel.casinodice.business.AnySeven;
import com.kenfogel.casinodice.business.FieldBet;
import com.kenfogel.casinodice.business.PassLine;
import com.kenfogel.casinodice.enums.OutComes;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.util.converter.BigDecimalStringConverter;

/**
 * 'CasinoFXML.fxml' Controller Class
 *
 * @author Ken Fogel
 */
public class CasinoFXMLController {

    private final BankRoll bankRoll;
    private final Die die1;
    private final Die die2;
    private final AnySeven anySeven;
    private final FieldBet fieldBet;
    private final PassLine passLine;

    private enum Game {
        ANY7, FIELDBET, PASSLINE, PASSLINEPOINT, NONE
    };
    private Game currentGame;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="namePlayer"
    private TextField namePlayer; // Value injected by FXMLLoader

    @FXML // fx:id="emailPlayer"
    private TextField emailPlayer; // Value injected by FXMLLoader

    @FXML // fx:id="amountMoney"
    private TextField amountMoney; // Value injected by FXMLLoader

    @FXML // fx:id="casinoButton"
    private Button casinoButton; // Value injected by FXMLLoader

    @FXML // fx:id="anyButton"
    private Button anyButton; // Value injected by FXMLLoader

    @FXML // fx:id="fieldButton"
    private Button fieldButton; // Value injected by FXMLLoader

    @FXML // fx:id="passButton"
    private Button passButton; // Value injected by FXMLLoader

    @FXML // fx:id="betAmount"
    private TextField betAmount; // Value injected by FXMLLoader

    @FXML // fx:id="diceRoll"
    private Button diceRoll; // Value injected by FXMLLoader

    @FXML // fx:id="pointText"
    private TextField pointText; // Value injected by FXMLLoader

    @FXML // fx:id="outComeText"
    private Label outComeText; // Value injected by FXMLLoader

    @FXML // fx:id="die1Text"
    private Label die1Text; // Value injected by FXMLLoader

    @FXML // fx:id="die2Text"
    private Label die2Text; // Value injected by FXMLLoader

    public CasinoFXMLController() {
        bankRoll = new BankRoll();
        die1 = new Die();
        die2 = new Die();
        anySeven = new AnySeven();
        fieldBet = new FieldBet();
        passLine = new PassLine();
        currentGame = Game.NONE;

    }
    
//    public void setBankRoll(BankRoll bankRoll) {
//      this.bankRoll = bankRoll;  
//    }
        

    @FXML
    void enterCasino(ActionEvent event) {
        if (bankRoll.getPlayerName().length() > 0
                && bankRoll.getPlayerEmailAddress().length() > 0
                && bankRoll.getMoney().doubleValue() > 0) {
            enableGamePlay(true);
        }
    }

    @FXML
    void playAny(ActionEvent event) {
        currentGame = Game.ANY7;
        clearGameInfo();
    }

    @FXML
    void playField(ActionEvent event) {
        currentGame = Game.FIELDBET;
        clearGameInfo();
    }

    @FXML
    void playPass(ActionEvent event) {
        currentGame = Game.PASSLINE;
        clearGameInfo();
    }

    @FXML
    void rollDice(ActionEvent event) {
        if (bankRoll.getMoneyBet().doubleValue() > 0 && !currentGame.equals(Game.NONE)) {
            doTheDiceRoll();
            switch (currentGame) {
                case ANY7 -> playAnySevenGame();
                case FIELDBET -> playFieldBetGame();
                case PASSLINE -> playPassLineGame();
                case PASSLINEPOINT -> playPassLinePointGame();

            }
        }
    }

    /**
     * Clear labels
     */
    private void clearGameInfo() {
        die1Text.setText("");
        die2Text.setText("");
        outComeText.setText("");
        pointText.setText("");
    }

    /**
     * Roll two die and display the values
     */
    private void doTheDiceRoll() {
        //die1.rollTheDie();
        //die2.rollTheDie();
        die1Text.setText(die1.rollTheDie() + "");
        die2Text.setText(die2.rollTheDie() + "");
    }

    /**
     * Pass Line game control
     */
    private void playPassLineGame() {
        OutComes outcome = passLine.playPassLine(die1.getDie() + die2.getDie());
        pointText.setText("");
        switch (outcome) {
            case WIN_SINGLE -> {
                outComeText.setText(resources.getString("WIN"));
                bankRoll.add(bankRoll.getMoneyBet());
            }
            case LOSE -> {
                outComeText.setText(resources.getString("LOSE"));
                bankRoll.subtract(bankRoll.getMoneyBet());
            }
            case ROLL_AGAIN -> {
                currentGame = Game.PASSLINEPOINT;
                pointText.setText(die1.getDie() + die2.getDie() + "");
                outComeText.setText(resources.getString("ROLLAGAIN"));
                disableForPassLinePointGame(true);
            }
        }
    }

    /**
     * During Pass Line point play certain fields and buttons muts be disabled
     * or made un-editable
     *
     * @param disable
     */
    private void disableForPassLinePointGame(Boolean disable) {
        if (disable) {
            anyButton.setDisable(true);
            fieldButton.setDisable(true);
            passButton.setDisable(true);
            betAmount.setEditable(false);
        } else {
            anyButton.setDisable(false);
            fieldButton.setDisable(false);
            passButton.setDisable(false);
            betAmount.setEditable(true);
            currentGame = Game.PASSLINE;
        }

    }

    /**
     * Pass Line game control when playing for point
     */
    private void playPassLinePointGame() {

        OutComes outcome;
        doTheDiceRoll();
        outcome = passLine.playForPoint(die1.getDie() + die2.getDie());
        switch (outcome) {
            case LOSE -> {
                outComeText.setText(resources.getString("LOSE"));
                bankRoll.subtract(bankRoll.getMoneyBet());
                //currentGame = Game.PASSLINE;
                disableForPassLinePointGame(false);
            }
            case WIN_SINGLE -> {
                outComeText.setText(resources.getString("WIN"));
                bankRoll.add(bankRoll.getMoneyBet());
                //currentGame = Game.PASSLINE;
                disableForPassLinePointGame(false);
            }
            default -> outComeText.setText(resources.getString("ROLLAGAIN"));
        }
    }

    /**
     * Any7 game control
     */
    private void playAnySevenGame() {
        OutComes outcome = anySeven.playAnySeven(die1.getDie() + die2.getDie());
        if (outcome == OutComes.WIN_QUADRUPLE) {
            outComeText.setText(resources.getString("WIN"));
            bankRoll.add(bankRoll.getMoneyBet().multiply(new BigDecimal("4")));
        } else {
            outComeText.setText(resources.getString("LOSE"));
            bankRoll.subtract(bankRoll.getMoneyBet());
        }
    }

    /**
     * Field Bet game control
     */
    private void playFieldBetGame() {
        OutComes outcome = fieldBet.playFieldBet(die1.getDie() + die2.getDie());
        switch (outcome) {
            case WIN_SINGLE -> {
                outComeText.setText(resources.getString("WIN"));
                bankRoll.add(bankRoll.getMoneyBet());
            }
            case WIN_DOUBLE -> {
                outComeText.setText(resources.getString("WIN"));
                bankRoll.add(bankRoll.getMoneyBet().multiply(new BigDecimal("2")));
            }
            case WIN_TRIPLE -> {
                outComeText.setText(resources.getString("WIN"));
                bankRoll.add(bankRoll.getMoneyBet().multiply(new BigDecimal("3")));
            }
            case LOSE -> {
                outComeText.setText(resources.getString("LOSE"));
                bankRoll.subtract(bankRoll.getMoneyBet());
            }
        }

    }

    /**
     * Until you enter a name, email address and initial bankroll disable other
     * game components. Once filled in these fields become disabled and the game
     * fields are enabled.
     *
     * @param enable
     */
    private void enableGamePlay(Boolean enable) {
        if (enable) {
            namePlayer.setEditable(false);
            emailPlayer.setEditable(false);
            amountMoney.setEditable(false);
            casinoButton.setDisable(true);
            anyButton.setDisable(false);
            fieldButton.setDisable(false);
            passButton.setDisable(false);
            betAmount.setEditable(true);
            diceRoll.setDisable(false);
        } else {
            namePlayer.setEditable(true);
            emailPlayer.setEditable(true);
            amountMoney.setEditable(true);
            casinoButton.setDisable(false);
            anyButton.setDisable(true);
            fieldButton.setDisable(true);
            passButton.setDisable(true);
            betAmount.setEditable(false);
            diceRoll.setDisable(true);
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert namePlayer != null : "fx:id=\"namePlayer\" was not injected: check your FXML file 'CasinoFXML.fxml'.";
        assert emailPlayer != null : "fx:id=\"emailPlayer\" was not injected: check your FXML file 'CasinoFXML.fxml'.";
        assert amountMoney != null : "fx:id=\"amountMoney\" was not injected: check your FXML file 'CasinoFXML.fxml'.";
        assert casinoButton != null : "fx:id=\"casinoButton\" was not injected: check your FXML file 'CasinoFXML.fxml'.";
        assert anyButton != null : "fx:id=\"anyButton\" was not injected: check your FXML file 'CasinoFXML.fxml'.";
        assert fieldButton != null : "fx:id=\"fieldButton\" was not injected: check your FXML file 'CasinoFXML.fxml'.";
        assert passButton != null : "fx:id=\"passButton\" was not injected: check your FXML file 'CasinoFXML.fxml'.";
        assert betAmount != null : "fx:id=\"betAmount\" was not injected: check your FXML file 'CasinoFXML.fxml'.";
        assert diceRoll != null : "fx:id=\"diceRoll\" was not injected: check your FXML file 'CasinoFXML.fxml'.";
        assert pointText != null : "fx:id=\"pointText\" was not injected: check your FXML file 'CasinoFXML.fxml'.";
        assert outComeText != null : "fx:id=\"outComeText\" was not injected: check your FXML file 'CasinoFXML.fxml'.";
        assert die1Text != null : "fx:id=\"die1Text\" was not injected: check your FXML file 'CasinoFXML.fxml'.";
        assert die2Text != null : "fx:id=\"die2Text\" was not injected: check your FXML file 'CasinoFXML.fxml'.";

        Bindings.bindBidirectional(namePlayer.textProperty(), bankRoll.playerNameProperty());
        Bindings.bindBidirectional(emailPlayer.textProperty(), bankRoll.playerEmailAddressProperty());
        Bindings.bindBidirectional(amountMoney.textProperty(), bankRoll.moneyProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(betAmount.textProperty(), bankRoll.moneyBetProperty(), new BigDecimalStringConverter());

        enableGamePlay(false);
    }
}
